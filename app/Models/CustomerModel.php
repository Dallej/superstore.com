<?php namespace App\Models;

use CodeIgniter\Model;

class CustomerModel extends Model{
    protected $table ='customer';

    protected $allowedFields=['firstname','lastname','email',
    'mobilenumber','address','postnumber','postoffice',
    'username','password'
];
}