<?php namespace App\Models;

use CodeIgniter\Model;

class ReviewModel extends Model {
	 
    public function reviewToDatabase($inputData) {           
        $db = db_connect();
        $builder = $db->table('review');
        $builder->insert($inputData);
    }

    public function getAllReviewsForThisProduct($id) {
        $db = db_connect();
        $builder = $db->table('review');
        $builder->where('product_ID', $id);
        $builder->orderBy('ID', 'desc');
        $query = $builder->get();
        return $query->getResult();
    }

    public function getAllReviews(){
        $db = db_connect();
        $builder = $db->table('review');
        $builder->orderBy('ID');
        $query = $builder->get();
        return $query->getResult();
    }
    public function remove($value) {
        $db = db_connect();
        $builder = $db->table('review');
        $builder->where('ID', $value);
        $builder->delete();
    }

}