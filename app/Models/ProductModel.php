<?php namespace App\Models;

use CodeIgniter\Model;

class ProductModel extends Model {

    protected $table      = 'product';
    protected $primaryKey = 'ID';

    protected $allowedFields = ['ID','name','description','picture','price','storageamount', 'sale', 'category_ID'];

    // Etsii kaikki tuotteet, jotka ovat asiakkaille näkyvissä
    public function getAllProducts() {
        $db = db_connect();
        $builder = $db->table('product');
        $builder->where('visible', true);
        $builder->orderBy('ID');
        $query = $builder->get();
        return $query->getResult();
    }

    // Etsii kaikki tuotteet, myös asiakkailta piilotetut
    public function getAllProductsDespiteVisibility() {
        $db = db_connect();
        $builder = $db->table('product');
        $builder->orderBy('ID');
        $query = $builder->get();
        return $query->getResult();
    }

    public function getAllCategories() {
        $db = db_connect();
        $builder = $db->table('category');       
        $builder->orderBy('ID');
        $query = $builder->get();
        return $query->getResult();
    }

    public function addCategoryToDatabase($name) {
        $db = db_connect();
        $builder = $db->table('category');
        
        $data = [
            'name' => $name,
        ];

        $builder->insert($data);
    } 

    public function getCategoryIDs() {
        $db = db_connect();
        $builder = $db->table('category');       
        $builder->select('ID');
        $query = $builder->get();
        return $query->getResult();
    }

    public function getProductCategoryIDs() {
        $db = db_connect();
        $builder = $db->table('product');       
        $builder->select('category_ID');
        $query = $builder->get();
        return $query->getResult();
    }

    public function addProductToDatabase($inputData) {
        $db = db_connect();
        $builder = $db->table('product');
        $builder->insert($inputData);
    }

    public function remove($id, $table) {
        $db = db_connect();
        $builder = $db->table($table);
        $builder->where('id', $id);
        $builder->delete();
    }

    public function getProductOrCategory($id, $table) {
        $db = db_connect();
        $builder = $db->table($table);       
        $builder->where('ID', $id);
        $query = $builder->get();
        return $query->getResult();
    }

    public function editCategoryInDatabase($id, $name) {
        $db = db_connect();
        $builder = $db->table('category');
        
        $data = [
            'name' => $name,
        ];

        $builder->where('id', $id);
        $builder->update($data);
    }

    public function editProductInDatabase($id, $inputData) {
        $db = db_connect();
        $builder = $db->table('product');
        $builder->where('id', $id);
        $builder->update($inputData);
    }

    public function getProductsByCategory($id) {
        $db = db_connect();
        $builder = $db->table('product');
        $builder->where('category_ID', $id);
        $builder->where('visible', true);
        $builder->orderBy('ID');
        $query = $builder->get();
        return $query->getResult();
    }

    public function getProductsByWords($criteria, $id) {
        $db = db_connect();
        $builder = $db->table('product');
        foreach ($criteria as $word):
        if ($id != 0) {
            $builder->groupStart()
                        ->where('category_ID', $id)
                            ->groupStart()
                                ->like('name', $word, 'both')
                                ->orlike('price', $word, 'both')
                                ->orlike('description', $word, 'both')
                            ->groupEnd()
                    ->groupEnd();
        }
        else {
            $builder->groupStart()
                        ->like('name', $word, 'both')
                        ->orlike('price', $word, 'both')
                        ->orlike('description', $word, 'both')
                    ->groupEnd();
        }
        endforeach;
        $builder->orderBy('ID');
        $query = $builder->get();
        return $query->getResult();
    }

    public function changeVisibility($id) {
        $db = db_connect();
        $builder = $db->table('product');
        $builder->where('ID', $id);

        if ($builder) {
            $query = $builder->get();
            $row = $query->getRow();
        }
        if ($row) {
            if (($row->visible == false)) {
                $data = [
                    'visible' => true,
                ];
            }
            else {
                $data = [
                    'visible' => false,
                ];
            }
        }
        $builder->where('ID', $id);
        $builder->update($data);
    }

//ostoskorin tuotteet 
    public function getCartProducts($cart){
        $result = array();
        $db = db_connect();
        $builder = $this->table("product");
        $builder->whereIn("id", $cart);
        $query = $builder->get();
        $product = $query->getRowArray();
        $this->add_product($product,$result);
        return $query->getResultArray();
    }

//tuotteiden lisääminen ostoskorissa
    private function add_product($product,&$array){
        for($i = 0; $i < count($array); $i++){
            if($array[$i]['id'] === $product['id']){
                $array[$i]['amount'] = $array[$i]['amount'] + 1;
                return;
            }
        }
        $product['amount'] = 1;
        array_push($array,$product);
    }

}