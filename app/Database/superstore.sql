
/*Create database and use it*/
DROP DATABASE IF EXISTS superstore;
CREATE DATABASE IF NOT EXISTS superstore;
USE superstore;


/*all drop table lines*/
DROP TABLE IF EXISTS Category; 
DROP TABLE IF EXISTS Customer; 
DROP TABLE IF EXISTS Product; 
DROP TABLE IF EXISTS OrderLine;  
DROP TABLE IF EXISTS OrderList;  


/*Creating customer table*/
CREATE TABLE Customer ( 

    ID INT NOT NULL AUTO_INCREMENT, 

    firstname VARCHAR(50) NOT NULL, 

    lastname VARCHAR(50) NOT NULL, 

    email VARCHAR(255) NOT NULL, 

    mobilenumber INT(15), 

    address VARCHAR(25), 

    postnumber INT(8), 

    

    postoffice VARCHAR(25), 

    username VARCHAR(50) NOT NULL,

    password VARCHAR(100) NOT NULL,

    PRIMARY KEY (ID) 
); 

 /*Creating Category table*/
 CREATE TABLE Category ( 

    ID INT NOT NULL AUTO_INCREMENT, 

    name VARCHAR(25) NOT NULL, 

    PRIMARY KEY (ID) 
); 

/*Creating Product table*/
CREATE TABLE Product ( 

    ID INT(11) NOT NULL AUTO_INCREMENT, 

    name VARCHAR(255) NOT NULL, 

    description TEXT(255), 

    picture VARCHAR(50) DEFAULT 'default.img', 

    price DECIMAL(7,2) NOT NULL,

    storageamount INT(11) NOT NULL, 

    sale TINYINT(3) DEFAULT 0,

    visible boolean NOT NULL default 1,

    created timestamp default current_timestamp,

    PRIMARY KEY (ID),
	category_ID INT,

    FOREIGN KEY (category_ID) REFERENCES category(ID)

);

/*Creating OrderList table*/
CREATE TABLE OrderList (
	ID INT(11) NOT NULL AUTO_INCREMENT,
	status ENUM('Ordered','Paid','Shipped'),
	date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	customer_ID INT NOT NULL,
	PRIMARY KEY (ID),
	FOREIGN KEY (customer_ID) REFERENCES Customer(ID)
);


/*Creating OrderLine table*/
CREATE TABLE OrderLine  (
    ID INT(11) NOT NULL AUTO_INCREMENT,
    amount INT(11) NOT NULL,
    product_ID INT(11) NOT NULL,
    order_ID INT(11) NOT NULL,
    FOREIGN KEY (product_ID) REFERENCES Product(ID),
    FOREIGN KEY (order_ID) REFERENCES OrderList(ID),
    PRIMARY KEY (ID)

);
/*Creating contact table*/
CREATE TABLE contact (
    ID INT(11) NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	mail VARCHAR(255),
	message TEXT NOT NULL,
    isread boolean NOT NULL default 0,
    saved timestamp default current_timestamp ON UPDATE current_timestamp,
    PRIMARY KEY (ID)
);
/*Creating review table*/
CREATE TABLE review (
	ID INT(11) NOT NULL AUTO_INCREMENT,
	rating INT(1),
	comment TEXT NOT NULL,
	writer VARCHAR(25) NOT NULL,
    date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (ID),
	product_ID INT(11) NOT NULL,
	FOREIGN KEY (product_ID) REFERENCES Product(ID)
);

/*INSERT CATECORY*/

INSERT INTO Category (name)
VALUES ('Components');

INSERT INTO Category (name)
VALUES ('Peripherals');

INSERT INTO Category (name)
VALUES ('Computers');


/*INSERT PRODUCT*/

/* fields for product ID AUTO_INCREMENT, name description picture price storageamount category_ID */

/* INSERT INTO Product (name,description,picture,price,storageamount,category_ID)
VALUES ('NAME',"DESC","picturename.jpg",$$$,storeageamount,category ID); */

/*CATECORY 1 components*/

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Asus Dual GeForce GTX 1060',"New Turing Architecture, 6GB 192-bit GDDR5, Dual slot, 4K / HDR / VR Ready, 
3 x DisplayPort 1.4, 1 x HDMI 2.0b, Boost Clock 1785 MHz","asus1060.jpg",300,25,25,1);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Asus Dual GeForce GTX 1070',"New Turing Architecture, 8GB 192-bit GDDR5, Dual slot, 4K / HDR / VR Ready, 
3 x DisplayPort 1.4, 1 x HDMI 2.0b, Boost Clock 1785 MHz","asus1070.jpg",400,25,0,1);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Asus Dual GeForce GTX 1080',"New Turing Architecture, 8GB 192-bit GDDR5, Dual slot, 4K / HDR / VR Ready, 
3 x DisplayPort 1.4, 1 x HDMI 2.0b, Boost Clock 1900 MHz","asus1080.jpg",500,25,0,1);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Intel Core i9-9900K Desktop Processor',"8 Cores up to 5.0 GHz Turbo unlocked LGA1151 300 Series 95W","9900k.jpg",600,0,10,1);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('AMD Ryzen 5 3600',"6-Core, 12-Thread Unlocked Desktop Processor with Wraith Stealth Cooler","amd3600.jpg",200,10,50,1);


/*CATECORY 2 pheripherals*/

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Ducky Shine 7',"Switch type: Cherry MX Brown Zinc alloy top case is 3x stronger than 
aluminum Brand new design with premium build quality Supports Ducky Macro 2.0, the most powerful hardware available in the market Keycaps: 
Black Double Shot PBT","duckyshine7.jpg",199,25,0,2);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
 VALUES ('Logitech Desktop Keyboard MK120 & USB Mouse',"Durable, Comfortable, USB Mouse and keyboard Combo","logik120.jpg",30,0,0,2);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('HP P Series P274 27"',"Full HD 1920 x 1080 LED LCD Anti-glare Monitor","HP27.jpg",150,10,15,2);

/*CATECORY 3 computers*/

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('ROG Zephyrus M Thin and Portable Gaming Laptop, 15.6”',"240Hz FHD IPS, NVIDIA GeForce RTX 2070, Intel Core i7-9750H, 
16GB DDR4 RAM, 1TB PCIe SSD, Per-Key RGB, Windows 10 Home, GU502GW-AH76.","roglaptop.jpg",1800,0,0,3);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Lenovo ThinkPad T470 Laptop Computer 14"',"FHD Display 1920x1080, Intel Dual Core i5-6300U, 16GB RAM, 512GB SSD NVMe, W10P, Business Laptop","lenovot470.jpg",800,5,0,3);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('HP 8300 Elite Desktop Computer',"Intel Core i5-3470 3.2GHz Quad-Core, 8GB RAM, 500GB SATA, Windows 10 Pro 64-Bit, USB 3.0, Display Port","HP8300.jpg",200,5,75,3);


/*ADMIN ACCOUNT*/

INSERT INTO customer (firstname,lastname,email,username,password)
VALUES ("siteadmin","siteadmin","siteadmin@test.com","admin123","$2y$10$0rIDS.RXURHEVRY6IXK6YOtwN.lTGfH2j.yGPAfXaiJIlWMTsyrQm");

/*INSERT CUSTOMER*/

INSERT INTO customer (firstname,lastname,email,username,password)
VALUES ("testfirst","testlast","testmail@test.com","test","test");

/*INSERT REVIEW*/
INSERT INTO review (rating, comment, writer, product_ID) 
VALUES ('5', 'test review', 'test writer', '5');

/*INSERT CONTACT*/
INSERT INTO contact (name, mail, message) 
VALUES ('Test jack', 'jacks@mail.com', 'I would like to buy 1000 products and we make a deal.');
  

/*PYLLY*/

INSERT INTO Category (name)
VALUES ('Huumeet');

INSERT INTO Category (name)
VALUES ('Neekerit');

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Oopiumia',"Parasta laatua","munkki.jpg",40000,1000,0,4);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Nestori Neekeri',"Ahkera, joskin hieman hidasjärkinen orja. Saatavana tummana tai tosi tummana versiona.","nekru1.png",2,500000,99,5);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Neekeri 47',"Maailman paras salamurhaaja","nekru2.png",99999,0,0,5);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Confused Nigga',"Erittäin ymmällään oleva neekeri","nekru3.png",420,666,42,5);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Pyssynekru',"Pystyy tappamaan kaikki valkoiset","nekru4.jpg",10000,1,0,5);

INSERT INTO Product (name,description,picture,price,storageamount,sale,category_ID)
VALUES ('Crying Nigger',"QQ","nekru5.gif",777,77,77,5);

INSERT INTO review (rating, comment, title, writer, product_ID) 
VALUES ('0', 'DJANGOOO! EI YKS PYSSYNEEKERI VOI TAPPAA KAIKKIA VALKOISIA', 'test title', 'Stephen', '16');

INSERT INTO review (rating, comment, title, writer, product_ID) 
VALUES ('5', 'Nitun veekeri', 'test title', 'Basisti', '16');

INSERT INTO review (rating, comment, title, writer, product_ID) 
VALUES ('3', 'Kallokuopat on syvät', 'test title', 'Ihmisten kalastaja', '13');

INSERT INTO review (rating, comment, title, writer, product_ID) 
VALUES ('5', 'Mulla on kolme, suosittelen', 'test title', 'Paavi', '13');

INSERT INTO review (rating, comment, title, writer, product_ID) 
VALUES ('5', 'Mulla on näitä koko pajallinen, ovat kyllä loistavia', 'test title', 'Sinter Klaas', '13');