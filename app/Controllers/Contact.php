<?php namespace App\Controllers;

use App\Models\ProductModel;
use App\Models\ContactModel;

// Contact page
class Contact extends BaseController {

    public function __construct() {
		$session = \Config\Services::session();
		$session->start();

		$this->model = new ProductModel();
		$this->data['products'] = $this->model->getAllProducts();
    $this->data['categories'] = $this->model->getAllCategories();
    

    //$this->load->database();
    //$this->load->model('ContactModel');
    }
    
    public function index() {
		$data = $this->data;
		$data['totalProducts'] = count($data['products']);

		echo view('templates/header', $data);
		echo view('contact_view', $data);
		echo view('templates/footer');
    }

    public function savedata(){
		
		$inputData = [
		'name' => $this->request->getPost('name'),
		'mail' => $this->request->getPost('mail'),
		'message' => $this->request->getPost('message'),
	];

		print_r($inputData);

		echo "<h4>Your message was succesfully sent.</h4>";

		if (!isset($_SESSION['contact'])){
			$_SESSION['contact']=array(); 
		}
	   
	   array_push($_SESSION['contact'], $inputData);
	   

	   	$ContactModel = new ContactModel();
		$ContactModel->savecontact($inputData);

		return redirect('/');
	
      
	}

}