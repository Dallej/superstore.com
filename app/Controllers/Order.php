<?php namespace App\Controllers;

use App\Models\ProductModel;

use App\Models\OrderModel;

class Order extends BaseController
{
    public function __construct(){
        $session =\Config\Services::session();
        $session->start();
        $this->model = new ProductModel();
		$this->data['products'] = $this->model->getAllProducts();
        $this->data['categories'] = $this->model->getAllCategories();
    }

    public function index(){
        $model = $this->model;
		$data = $this->data;
        echo view ('templates/header', $data);
        echo view ('order_view', $data);
        echo view ('templates/footer');
    }

    public function purchase(){
        //tallentaa asiakkaan tiedot
        $customer= [
            'firstname'=>$this->request->getPost('firstname'),
            'lastname'=>$this->request->getPost('lastname'),
            'email'=>$this->request->getPost('email'),
            'mobilenumber'=>$this->request->getPost('mobilenumber'),
            'address'=>$this->request->getPost('address'),
            'postnumber'=>$this->request->getPost('postnumber'),
            'postoffice'=>$this->request->getPost('postoffice'),
            'username'=>$this->request->getPost('username'),
            'password'=>$this->request->getPost('password')
        ];
    // print_r($customer);
   $orderModel =new OrderModel();
       
    //tallentaa tilaustiedot ja tilausrivit
     $order=$orderModel->archive($customer,$_SESSION['cart']);
    
     if($order == true){

     

       //tyhjennä ostoskori

       unset($_SESSION['cart']);
        
       //redirect etusivulle
       return redirect("/");

     }
     else{
         //redirect etusivulle
        return redirect ("/");

     }
    }
}