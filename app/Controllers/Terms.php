<?php namespace App\Controllers;

use App\Models\ProductModel;

class Terms extends BaseController
{
	public function __construct() {
		$session = \Config\Services::session();
		$session->start();

		$this->model = new ProductModel();
		$this->data['categories'] = $this->model->getAllCategories();
	}
		
	public function index() {
        $data = $this->data;

		echo view('templates/header', $data);
		echo view('terms_view', $data);
		echo view('templates/footer');
	}

}
