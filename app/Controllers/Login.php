<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Models\ProductModel;

// Handlaa loginin ja rekisteröinnin.
// Ongelma(?): Login modaali vaihtaa submittien yhteydessä aina etusivulle
// Ongelma: Ei käytetä return redirectia, mikä ei muuta URL:ia. Eli jos refreshaa, funktio toistuu vaikkei haluaisi.
class Login extends BaseController {

	public function __construct() {
		$session = \Config\Services::session();
		$session->start();

		$this->model = new ProductModel();
		$this->data['products'] = $this->model->getAllProducts();
                $this->data['categories'] = $this->model->getAllCategories();

                $this->data['totalProducts'] = count($this->data['products']);
                
                // Tämä carusellia varten, aina kaikki tuotteet näkyvillä
		$this->data['ads'] = $this->model->getAllProducts();
	}
        
        public function loginSubmit() {
                $model = $this->model;
                $data = $this->data;

                if (!$this->validate([
                        'username' => 'required|min_length[8]|max_length[50]',
                        'password' => 'required|min_length[8]|max_length[50]',
                ])){    
                        $data['message'] = "An error occurred.";
                        
                        echo view('templates/header', $data);
                        echo view('frontpage_view', $data);
                        echo view('templates/footer');
                        echo
                        '<script type="text/JavaScript">
                                $(".message").addClass("bg-danger");
                                $("#login").modal({backdrop: "static", keyboard: true});
                        </script>';
                }
                else {
                        $username = $this->request->getVar('username');
                        $password = $this->request->getVar('password');

                        $model = new UserModel();

                        $user = $model->loginCheck($username, $password);

                        if ($user) {
                                // LOG IN TAPAHTUU
                                $_SESSION['user'] = $user;
                                return redirect('/');
                        }
                        else {
                                $data['message'] = "Something went wrong. Make sure you spell the information correctly.";
                
                                echo view('templates/header', $data);
                                echo view('frontpage_view', $data);
                                echo view('templates/footer');
                                echo
                                '<script type="text/JavaScript">
                                        $(".message").addClass("bg-danger");
                                        $("#login").modal({backdrop: "static", keyboard: true});
                                </script>';
                        }
                }
        }

        public function registerSubmit() {   
                $model = $this->model;
                $data = $this->data;

                if (!$this->validate([
                        'username' => 'required|min_length[8]|max_length[50]|is_unique[customer.username]',
                        'password' => 'required|min_length[8]|max_length[50]',
                        'passwordconfirm' => 'required|min_length[8]|max_length[50]|matches[password]',
                ])) {
                        $data['message'] = "Username is already in use or password didn't match the confirm field.";

                        echo view('templates/header', $data);
                        echo view('frontpage_view', $data);
                        echo view('templates/footer');
                        echo
                        '<script type="text/JavaScript">
                                $(".message").addClass("bg-danger");
                                $("#register").modal({backdrop: "static", keyboard: true});
                        </script>';
                }
                else {  
                        $inputData = [
                                'username' => $this->request->getVar('username'),
                                'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
                                'firstname' => $this->request->getVar('firstname'),
                                'lastname' => $this->request->getVar('lastname'),
                                'email' => $this->request->getVar('email'),
                                'mobilenumber'  => $this->request->getVar('mobilenumber'),
                                'address' => $this->request->getVar('address'),
                                'postnumber' => $this->request->getVar('postnumber'),
                                'postoffice' => $this->request->getVar('postoffice')
                        ];

                        $model = new UserModel();

                        $model->registerRun($inputData);

                        $data['message'] = "Registration success!";

                        echo view('templates/header', $data);
                        echo view('frontpage_view', $data);
                        echo view('templates/footer');
                        echo
                        '<script type="text/JavaScript">
                                $(".message").addClass("bg-success");
                                $("#login").modal({backdrop: "static", keyboard: true});
                        </script>';
                }
        }

        public function logout() {
                $data = $this->data;

                unset($_SESSION['user']);
                
                echo view('templates/header', $data);
                echo view('frontpage_view', $data);
                echo view('templates/footer');
        }
}