<!-- Modaali, joka on sisällytetty headeriin -->
<div class="modal" tabindex="-1" role="dialog" id="login">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Login</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <p>Type in your account's username and password to proceed.</p>
                        <p>(Admin access = username: admin123 password: admin123)</p>
                        <p>
                            <span class="message">
                                <?php
                                    if (isset($message)) {
                                        echo $message;
                                    }
                                ?>
                            </span>
                        </p>
                        <?php
                        // Ei käytössä, nyt on manuaalisesti toteutettu virheilmoitus
                        // echo \Config\Services::validation()->listErrors();
                        ?>
                        <form action="/login/loginSubmit" class="bg-info border border-dark p-2" method="post">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Username:</label>
                                        <input class="form-control border border-dark" name="username" type="text"
                                            placeholder="Enter username" minlength="8" maxlength="50" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Password:</label>
                                        <input class="form-control border border-dark" name="password" type="password"
                                            placeholder="Enter password" minlength="8" maxlength="50" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center mb-2">
                                    <button type="submit"
                                        class="btn btn-md btn-light w-25 border border-dark">Login</button>
                                </div>
                            </div>
                        </form>
                        <p class="mt-2">Dont have an account yet? <a class="registerbutton" href="#">Click here
                                to
                                register</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>