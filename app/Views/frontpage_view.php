<div class="container-fluid">
    <?php include "roskaa.php"; ?>
    <div class="row">
        <div class="col-8 text-center text-white">
            <div class="frontpageheader pt-1 pb-1">
                <h2>Welcome to <a href="#" class="epilepsia">SuperStore.com</a></h2>
                <p>Where price & quality meet.
                    <a class="paskaa yllari" data-toggle="popover" data-content="Click to learn more about our store."
                        data-trigger="hover" data-placement="right" tabindex="-1"> <i
                            class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                    </a>
                </p>
            </div>
        </div>
        <div class="col-4 text-center">
            <div class="carousel slide" data-ride="carousel" data-interval="3000" data-pause="false">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <figure>
                            <figcaption class="text-white" style="background-color: #ff0000;">FUCK YOU CORONA!
                            </figcaption>
                            <img src="/img/freedelivery.jpg" class="img-fluid">
                        </figure>
                    </div>
                    <?php shuffle($ads); ?>
                    <?php foreach($ads as $key => $ad): ?>
                    <div class="carousel-item">
                        <figure>
                            <?php
                            if($key % 2 == 0){ 
                                echo '<figcaption class="text-white" style="background-color: #000000;">BEST QUALITY!</figcaption>';  
                            } 
                            else{ 
                                echo '<figcaption class="text-white" style="background-color: #ff0000;">BEST PRICES!</figcaption>';
                            }
                            ?>
                            <img src="/img/<?= $ad->picture; ?>" class="img-fluid">
                            <div class="pt-2 ad d-flex flex-column">
                                <h5><?= $ad->name; ?></h5>
                                <p class="font-weight-bold">
                                    <?= number_format($ad->price * ((100 - $ad->sale)/100), 2); ?> €</p>
                            </div>
                        </figure>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center pt-3 pb-2">
            <h3 class="productsheader">
                <?php
                if (isset($selectedCategory)) {
                    echo $selectedCategory[0]->name . ' (' . $totalProducts . ')';
                }
                else {
                    echo 'All Products' . ' (' . $totalProducts . ')';
                }
            ?></h3>
            <div class="row d-flex justify-content-center">
                <?php foreach($products as $product): ?>
                <div class="card border border-dark m-1" style="width: 15rem;">
                    <div class="imgframe">
                        <a href="<?=base_url('product/index/'.$product->ID) ?>">
                            <img class="img-fluid"src="/img/<?= $product->picture; ?>" alt="">
                        </a>
                    </div>
                    <div class="topleft"><i class="fa fa-star"></i></div>
                    <?php
                        // Jos tuote loppu, näytä loppuleima
                        if ($product->storageamount < 1) {
                            echo '<div class="outofstocktag">Out of stock!</div>';
                        }
                        // Jos alennuksessa, näytä alennusleima
                        if ($product->sale > 0) {
                            echo '<div class="saletag">- ' . $product->sale . ' %</div>';
                        }
                        // Jos tuote on luotu alle *jokin aika* sitten, näytä uutuusleima
                        // Aikavyöhyke vaikutti olevan projektissa väärä, joten tässä muunnos
                        date_default_timezone_set('Europe/Helsinki');
                        $time = strtotime($product->created);
                        $curtime = time();
                        // Määritetty aika
                        $waiting_time = 60*60*24;
                        if(($curtime-$time) < $waiting_time) {
                            echo '<div class="newtag">New!</div>';
                        }
                    ?>
                    <div class="card-body d-flex flex-column">
                        <a href="<?=base_url('product/index/'.$product->ID) ?>">
                            <h5 class="card-title"><?= $product->name; ?></h5>
                        </a>
                        <?php
                            if ($product->sale > 0) {
                                echo '<p class="card-text mt-auto" style="text-decoration:line-through 1px !important;">' . $product->price . ' €</p>';
                            }
                        ?>
                        <p class="card-text font-weight-bold mt-auto">
                            <?= number_format($product->price * ((100 - $product->sale)/100), 2); ?> €</p>
                        <form method="post" action="<?=base_url('Cart/Add/'.$product->ID) ?>">
                            <button <?php if ($product->storageamount < 1) {echo "disabled"; } ?>
                                class="btn btn-secondary mt-auto"><i class="fa fa-shopping-basket fa-md"></i> Add to
                                cart</button>
                        </form>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>