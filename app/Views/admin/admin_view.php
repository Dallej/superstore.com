<?php include "addcategory_modal.php" ?>
<?php include "remove_modal.php" ?>
<?php include "addimage_modal.php" ?>
<!-- Tulisi error, jos näitä ei ole määritetty. Siksi modaalit sisällytetty vain jos määritykset on tehty. -->
<?php 
if (isset($file)) {
    include "removeimage_modal.php";
}

if (isset($item1) &&isset($_SESSION['images'])) {
    include "editproduct_modal.php";
}

if (isset($item2)) {
    include "editcategory_modal.php";
}

if (isset($_SESSION['images'])) {
    include "addproduct_modal.php";
}
?>

<div class="container-fluid admin">
    <div class="row">
        <div class="col-12 pt-1 text-center text-white bg-dark adminheader">
            <h2>Admin Control Panel</h2>
            <p>Here the admin can view information and make changes to them.</p>
            <a href="#" class="btn btn-lg btn-danger mt-1 mr-1 mb-3 selection">Products</a>
            <a href="#" class="btn btn-lg btn-danger mt-1 mr-1 mb-3 selection">Categories</a>
            <a href="#" class="btn btn-lg btn-danger mt-1 mr-1 mb-3 selection">Images</a>
            <a href="#" class="btn btn-lg btn-danger mt-1 mr-1 mb-3 selection">Users</a>
            <a href="#" class="btn btn-lg btn-danger mt-1 mr-1 mb-3 selection">Orders</a>
            <a href="#" class="btn btn-lg btn-danger mt-1 mr-1 mb-3 selection">Messages</a>
            <a href="#" class="btn btn-lg btn-danger mt-1 mb-3 selection">Reviews</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pt-2 hidden" id="Products">
            <h3><i class="fa fa-cube"></i> Products</h3>
            <a href="#" class="btn btn-lg btn-danger mt-1 mb-3 addproduct">Add new product</a>
            <table class="table border border-dark">
                <thead class="thead-dark">
                    <th>
                        Product ID
                    </th>
                    <th>
                        Product Name
                    </th>
                    <th>
                        Category Name
                    </th>
                    <th>
                        Options
                    </th>
                </thead>
                <tbody class="bg-light">
                    <?php foreach($products as $product): ?>
                    <tr>
                        <td>
                            <?= $product->ID; ?>
                        </td>
                        <td>
                            <?= $product->name; ?>
                        </td>
                        <td>
                            <?php
                            foreach($categories as $category):
                            if ($category->ID === $product->category_ID) {
                                echo $category->name;
                                break;
                            }
                            endforeach;
                            ?>
                        </td>
                        <td>
                            <a href="/admin/editButtons/<?= $product->ID ?>/product"
                                class="btn btn-danger btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                            <a href="/admin/removeButtons/<?= $product->ID ?>/product"
                                class="btn btn-danger btn-sm"><i class="fa fa-eraser"></i> Remove</a>
                            <form action="/admin/changeProductVisibility/<?= $product->ID ?>" method="post">
                            <div class="form-check pt-1">
                                <input class="form-check-input" type="checkbox" value="" id="check" onChange="this.form.submit()" <?php if ($product->visible == true) {echo 'checked';} ?>>
                                <label class="form-check-label" for="check">
                                    Visible to customers
                                </label>
                            </div>
                            </form>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="col-12 pt-2 hidden" id="Categories">
            <h3><i class="fa fa-list"></i> Categories</h3>
            <a href="#" class="btn btn-lg btn-danger mt-1 mb-3 addcategory">Add new category</a>
            <table class="table border border-dark">
                <thead class="thead-dark">
                    <th>
                        Category ID
                    </th>
                    <th>
                        Category Name
                    </th>
                    <th>
                        Product Amount
                    </th>
                    <th>
                        Options
                    </th>
                </thead>
                <tbody class="bg-light">
                    <?php foreach($categories as $category): ?>
                    <tr>
                        <td>
                            <?= $category->ID; ?>
                        </td>
                        <td>
                            <?= $category->name; ?>
                        </td>
                        <td>
                            <?php
                            $amount = 0;
                            foreach($products as $product):
                                if ($category->ID === $product->category_ID) {
                                    $amount++;
                                }
                            endforeach;
                            echo $amount
                            ?>
                        </td>
                        <td>
                            <a href="/admin/editButtons/<?= $category->ID ?>/category"
                                class="btn btn-danger btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                            <a href="/admin/removeButtons/<?= $category->ID ?>/category"
                                class="btn btn-danger btn-sm"><i class="fa fa-eraser"></i> Remove</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="col-12 pt-2 hidden" id="Images">
            <h3><i class="fa fa-file-image-o"></i> Images</h3>
            <a href="#" class="btn btn-lg btn-danger mt-1 mb-3 addimage">Upload new image</a>
            <div class="row d-flex justify-content-left ml-2 mr-2 mb-3 images">
                <?php foreach ($_SESSION['images'] as $image): ?>
                <div class="text-center d-flex flex-column image">
                    <img style="max-width:9rem;" class="img-fluid" src="/img/<?= $image; ?>">
                    <div class="mt-auto">
                        <p><?= $image; ?></p>
                        <a href="/admin/removeImageButton/<?= pathinfo($image, PATHINFO_FILENAME); ?>/<?= pathinfo($image, PATHINFO_EXTENSION); ?>"
                            class="btn btn-danger btn-sm mt-auto w-100"><i class="fa fa-times"></i> Remove</a>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-12 pt-2 hidden" id="Users">
            <h3 class="mb-3"><i class="fa fa-users"></i> Users</h3>
            <p>Nothing to do here yet.</p>
            <table class="table border border-dark">
                <thead class="thead-dark">
                    <th>
                        Username
                    </th>
                    <th>
                        First Name
                    </th>
                    <th>
                        Last Name
                    </th>
                    <th>
                        Email
                    </th>
                    <th>
                        Mobile Number
                    </th>
                    <th>
                        Address
                    </th>
                    <th>
                        Post Number
                    </th>
                    <th>
                        Post Office
                    </th>
                </thead>
                <tbody class="bg-light">
                    <?php foreach($users as $user): ?>
                    <tr>
                        <td>
                            <?= $user->username; ?>
                        </td>
                        <td>
                            <?= $user->firstname; ?>
                        </td>
                        <td>
                            <?= $user->lastname; ?>
                        </td>
                        <td>
                            <?= $user->email; ?>
                        </td>
                        <td>
                            <?= $user->mobilenumber; ?>
                        </td>
                        <td>
                            <?= $user->address; ?>
                        </td>
                        <td>
                            <?= $user->postnumber; ?>
                        </td>
                        <td>
                            <?= $user->postoffice; ?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="col-12 pt-2 hidden" id="Orders">
            <h3 class="mb-3"><i class="fa fa-cubes"></i> Orders</h3>
            <p>Nothing to do here yet.</p>
            <table class="table border border-dark">
                <thead class="thead-dark">
                    <th>
                        Customer Name
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Order Date
                    </th>
                </thead>
                <tbody class="bg-light">
                    <?php foreach($users as $user): ?>
                    <tr>

                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="col-12 pt-2 hidden" id="Messages">
            <h3><i class="fa fa-envelope"></i> Messages</h3>
            <div class="row">
                <?php
                    $readMessages = 0;
                    $unreadMessages = 0;

                    foreach($contacts as $contact):
                        if ($contact->isread == true) {
                            $readMessages++;
                        }
                        else {
                            $unreadMessages++;
                        }
                    endforeach;
                ?>
                <div class="col-6">
                    <h4>Unread Messages (<?= $unreadMessages ?>)</h4>
                    <a href="/admin/markMessage/all" class="btn btn-danger btn-lg mb-3"><i class="fa fa-check"></i> Read All</a>
                    <?php foreach($contacts as $contact): ?>
                    <?php if ($contact->isread != true) { ?>
                    <div class="contact mb-3">
                        <h5>From <span><?= $contact->name ?></span></h5>
                        <p class="p-0 m-0">Email: <?= $contact->mail ?></p>
                        <p>Sent: <?= $contact->saved ?><p>
                                <p><?= $contact->message ?></p>
                                <a href="/admin/markMessage/<?= $contact->ID ?>" class="btn btn-danger btn-sm"><i class="fa fa-thumb-tack"></i> Mark as
                                    Read</a>
                    </div>
                    <?php } ?>
                    <?php endforeach;?>
                </div>
                <div class="col-6">
                    <h4>Read Messages (<?= $readMessages ?>)</h4>
                    <a href="/admin/removeMessage/all" class="btn btn-danger btn-lg mb-3"><i class="fa fa-ban"></i> Remove All</a>
                    <?php foreach($contacts as $contact): ?>
                    <?php if ($contact->isread == true) { ?>
                    <div class="contact mb-3">
                        <h5>From <span><?= $contact->name ?></span></h5>
                        <p class="p-0 m-0">Email: <?= $contact->mail ?></p>
                        <p>Marked as read: <?= $contact->saved ?><p>
                                <p><?= $contact->message ?></p>
                                <a href="/admin/removeMessage/<?= $contact->ID ?>"
                                    class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Remove</a>
                    </div>
                    <?php } ?>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        <div class="col-12 pt-2 hidden" id="Reviews">
            <h3 class="mb-3"><i class="fa fa-pencil-square"></i> Reviews</h3>
            <table class="table border border-dark">
                <thead class="thead-dark">
                    <th>
                        Review ID
                    </th>    
                    <th>
                        Rating
                    </th>
                    <th>
                        Writer
                    </th>
                    <th>
                        Comment
                    </th>
                    <th>
                        Date
                    </th>
                    <th>
                        Options
                    </th>
                </thead>
                <tbody class="bg-light">
                    <?php foreach($reviews as $review): ?> 
                       <!-- nappaa user foreach($users as $user): ?> -->
                    <tr>
                        <td>
                            <?= $review->ID; ?>
                        </td>
                        <td>
                            <?= $review->rating; ?>
                        </td>
                        <td>
                            <?= $review->writer; ?>
                        </td>
                        <td>
                            <?= $review->comment; ?>
                        </td>
                        <td>
                            <?= $review->date; ?>
                        </td>
                        <td>
                        <a href="/admin/removeReview/<?= $review->ID ?>"
                                class="btn btn-danger btn-sm"><i class="fa fa-eraser"></i> Remove</a>
                        </td>
                        
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>