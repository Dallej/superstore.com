<!-- Modaali, joka on sisällytetty admin_viewiin -->
<div class="modal" tabindex="-1" role="dialog" id="remove">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Remove <?= $table ?></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                    <?php
                        if ($table === 'product') {
                            foreach($products as $product):
                                if ($id === $product->ID) {
                                    echo '<p>' . $product->name . '</p>';
                                    break;
                                }
                            endforeach;
                        }
                        else {
                            foreach($categories as $category):
                                if ($id === $category->ID) {
                                    echo '<p>' . $category->name . '</p>';
                                    break;
                                }
                            endforeach;
                        }
                    ?>
                        <p>Are you sure you want to remove this <?= $table ?>?</p>
                        <p>
                            <span class="message">
                                <?php
                                    if (isset($message)) {
                                        echo $message;
                                    }
                                ?>
                            </span>
                        </p>
                        <div class="row">
                            <div class="col-12 mb-2">
                                <a href="/admin/removeProductOrCategory/<?= $id ?>/<?= $table ?>" class="btn btn-md btn-light border border-dark">Yes</a>
                                <button type="button" data-dismiss="modal" class="btn btn-md btn-light border border-dark">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>