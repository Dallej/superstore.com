<!-- Modaali, joka on sisällytetty admin_viewiin -->
<div class="modal" tabindex="-1" role="dialog" id="addimage">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Upload new image</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <p>Select a file and press upload.</p>
                        <p>
                            <span class="message">
                                <?php
                                    if (isset($message)) {
                                        echo $message;
                                    }
                                ?>
                            </span>
                        </p>
                        <?php
                        // Ei käytössä, nyt on manuaalisesti toteutettu virheilmoitus
                        // echo \Config\Services::validation()->listErrors();
                        ?>
                        <form action="/admin/addImage" class="bg-info border border-dark p-2" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>File:</label>
                                        <input class="form-control border border-dark" name="picture" type="file" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-2">
                                    <button type="submit"
                                        class="btn btn-md btn-light border border-dark">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>