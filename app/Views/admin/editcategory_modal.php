<!-- Modaali, joka on sisällytetty admin_viewiin -->
<div class="modal" tabindex="-1" role="dialog" id="editcategory">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Edit category</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <p>Apply changes in the fields and press submit.</p>
                        <p>
                            <span class="message">
                                <?php
                                    if (isset($message)) {
                                        echo $message;
                                    }
                                ?>
                            </span>
                        </p>
                        <?php
                        // Ei käytössä, nyt on manuaalisesti toteutettu virheilmoitus
                        // echo \Config\Services::validation()->listErrors();
                        ?>
                        <form action="/admin/editCategory/<?= $id ?>/<?= $table ?>"
                            class="bg-info border border-dark p-2"  method="post">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Category name:</label>
                                        <input class="form-control border border-dark" name="name" type="text"
                                            value="<?php echo $item2[0]->name ?>" minlength="1" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 mb-2">
                                    <button type="submit"
                                        class="btn btn-md btn-light border border-dark">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>