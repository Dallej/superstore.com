<div class="container-fluid">
        <div class="ml-5">
            <h2>Shopping Cart</h2>
        </div>
    <div class="row">
        <div class="col-md-6">
            <table>
                    
               

            <?php 
    //tarkistetaan onko ostoskorissa tuotteita

            if ($_SESSION['cart'] != null){ ?> 
                <tr>
                        <th></th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Amount</th>
                        <th></th>
                </tr>
                <?php foreach ($products as $product):  ?>
                    <tr>
                        <td><img class="img-fluid tdimg" src="/img/<?= $product['picture']?>"></td>
                        <td><?= $product['name']?></td>
                        <?php $counts = array_count_values($_SESSION['cart']); ?>
                        <td><?= number_format ($counts[$product['ID']] * $product['price'] * ((100 - $product['sale'])/100), 2); ?></td>
                        <td>
                        <div class="row d-flex justify-content-center my-auto pt-md-3">
                            <div class="col-md-2" >
                                <form method="post" action="<?=base_url('Cart/RemoveProduct/'.$product['ID']) ?>">
                                <button>-</button>
                                </form>
                            </div>
                            <div class="col-md-1 pr-md-4 pt-3 pt-md-0">
                                <?php
                                $amount = 0;
                                foreach($_SESSION['cart'] as $key => $value):
                                if($value === $product["ID"]){
                                    $amount++;
                                }
                                endforeach;?>
                                <input type="hidden" name="amount" value="<?=$amount?>">
                                <p id='<?=$product['ID']?>' class='pt-md-1 pl-md-2'><?=$amount?></p>
                            </div>
                            <div class="col-md-2  mr-md-1">
                                <form method="post" action="<?=base_url('Cart/AddProduct/'.$product['ID']) ?>">
                                    <button>+</button>
                                </form></td>
                            </div>
                        </div>
                        <td>
                            <div class="pixel">
                                <form method="post" action="<?=base_url('Cart/Remove/'.$product['ID']) ?>">
                                    <button>Remove</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                <?php endforeach ?>
                
            
            <?php } else{
                    echo "<div'><h3>Your shopping cart is empty</h3><div>";
            } 
            ?>

            </table>
        </div>
        
        
        <div class="col-md-6">
        <div class="row">
            <div class="col-6 align-bottom">
            <form method="post" action="<?= site_url('cart/empty')?>">
                <button>Remove all products</button>
            </form>
            </div>
            <div class="col-6">
            <form method="post" action="<?= base_url('Cart/order')?>">
                <button>Order</button>
            </form>
            </div>
        </div>
        
    </div>
</div>