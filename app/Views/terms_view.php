<div class="container">
    <h2>Information about us</h2>
        <h4>Store locations</h4>
<div class="row col-12">
        <div class="col-6">
        <h5>Superstore.com Helsinki</h5>
        <p>The Helsinki giant store and the 24-hour kiosk are located in Jätkäsaari, right next to the West Harbor.</p>
        <p>Helsinki store opening hours</p>
<p>Tyynenmerenkatu 11, 00220 Helsinki<br>

Opening hours:<br>
<ul>
    <li>Monday - Friday 9-21</li>
    <li>Saturday 9-21</li>
    <li>Sunday 11-19</li>
</ul>

 
Helsinki's 24-hour kiosk is always open!</p>
    </div>
        <div class="col-6">
        <h5>Superstore.com Pirkkala</h5>
        <p>The Pirkkala giant store is located in the Veska Shopping Center, right next to the Tampere Fair and Sports Center.</p>

        <p>Pirkkala store opening hours</p>
<p>Saapastie 2, 33950 Pirkkala<br>
Opening hours:<br>
<ul>
    <li>Monday - Friday 9-21</li>
    <li>Saturday 9-21</li>
    <li>Sunday 11-19</li>
</ul>

    </div>
</div>
    <h4 id="paymentoptions">Payment options</h4>
    <div class="row col-12">
      
      <p>You can buy safely from us! Superstore.com offers all the most common payment methods without compromising security.
           Our shopping cart has 128-bit SSL security. We do not store or retain customer payment card numbers.</p>

<p>Please note:<br>
In stores, a passport, driving license or identity card issued by a Finnish authority is required to prove identity. Identity card must be valid. Proof of identity must be provided when collecting a paid order online and when using the Scholarship and Klarna payment methods. In addition, scholarship and Klarna payment methods can accept a combination of a foreign passport and a Kela card, as long as the information in both documents matches each other and the order. Proxies are not accepted for scholarship and Klarna orders.
We do not accept Diners credit cards as a method of payment.
Invoicing and leasing sales are approved by our corporate sales.
You can also buy products tax-free from our store.</p>


<div class="col-6"><h4>Payment methods in stores</h4>
Cash<br>
Payment cards
(Visa, MasterCard, Visa Electron or American Express)<br>
Contactless payment<br>
(Most banking apps, Google Pay, Apple Pay)<br>
Credit<br>
Superstore.com gift card<br>
Klarna installment fee<br>
Klarna bill</p>
</div>
<div class="col-6">
<h4>Payment methods online</h4>
<p>Payment cards
(Visa, MasterCard, Visa Electron or American Express)<br>
Internet bank<br>
Cash on delivery<br>
Credit<br>
Superstore.com gift card<br>
Klarna installment fee<br>
Klarna bill</p>
      </div>

 </div>
</div>
