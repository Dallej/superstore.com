<div class="container-fluid">
        <div class="ml-5">
            <h2>Order page</h2>
        </div>
    <div class="row">
        <div class="col-5">
                <h4> Customer Information</h4>
                <form action="<?= site_url('order/purchase')?>" method="POST">
                <div class="col-8">
                <div class="form-group">
                    <label>First Name</label>
                    <input name="firstname" maxlength="50" class="form-control">
               </div>
                </div>
                <div class="col-8">
                <div class="form-group">
                    <label>Last Name</label>
                    <input name="lastname" maxlength="50" class="form-control">
                </div>
                </div>
                <div class="col-8">
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" maxlength="255" class="form-control">
                </div>
                </div>
                <div class="col-8">
                <div class="form-group">
                    <label>Mobilenumber</label>
                    <input name="mobilenumber" maxlength="15" class="form-control">
                    </div>
                </div>
                <div class="col-8">
                <div class="form-group">
                    <label>Address</label>
                    <input name="address" maxlength="25" class="form-control">
                </div>
                </div>
                <div class="col-8">
                <div class="form-group">
                    <label>Postnumber</label>
                    <input name="postnumber" maxlength="8" class="form-control">
                </div>
                </div>
                <div class="col-8">
                <div class="form-group">
                    <label>Postoffice</label>
                    <input name="postoffice" maxlength="50" class="form-control">
                </div>
                </div>
                <div class="col-8">
                <div class="form-group">
                    <label>Username</label>
                    <input name="username" maxlength="50" class="form-control">
                </div>
                </div>
                <div class="col-8">
                <div class="form-group">
                    <label>Password</label>
                    <input name="password" maxlength="100" class="form-control">
                </div>
                </div>
                <div class="">
                
                </div>
                
        </div>
        <div class="col-3">
            <h4>Shipping methods</h4>
            <div class="form-check">
  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
  <label class="form-check-label" for="exampleRadios1">
    Home delivery
  </label>
        </div>
        <div class="form-check">
  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
  <label class="form-check-label" for="exampleRadios1">
    Pick-up 
  </label>
    </div>
        </div>
        
            <div class="col-4">
            
                    <button class="btn btn-primary">Purchase Products</button> 
                    </form>
            </div>
        </div>
    </div>
</div>