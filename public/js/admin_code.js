$(document).ready(function () {
    
    // Kun painetaan addcategory buttonia, näytä addcategory modal (ja estä # merkin lisäys osoitteeseen)
    $(".addcategory").click(function () {
        event.preventDefault();
        $("#addcategory").modal({backdrop: 'static', keyboard: true});
    });

    // Kun painetaan addproduct buttonia, näytä addproduct modal (ja estä # merkin lisäys osoitteeseen)
    $(".addproduct").click(function () {
        event.preventDefault();
        $("#addproduct").modal({backdrop: 'static', keyboard: true});
    });

    // Kun painetaan addimage buttonia, näytä addimage modal (ja estä # merkin lisäys osoitteeseen)
    $(".addimage").click(function () {
        event.preventDefault();
        $("#addimage").modal({backdrop: 'static', keyboard: true});
    });
   
    // Kun painetaan näitä nappeja, poista mahdollinen ilmoitusviesti modaaleista
    $(".addcategory, .addproduct, .addimage").click(function () {
        $(".message").html('');
        $(".message").removeClass('bg-danger');
        $(".message").removeClass('bg-success');
    });

    // Kun painetaan admin vaihtoehtoja, näytä oikea valinta
    $(".selection").click(function () {
        let selected = $(this).html();
        event.preventDefault();
        $("#Products").hide();
        $("#Categories").hide();
        $("#Images").hide();
        $("#Users").hide();
        $("#Orders").hide();
        $("#Messages").hide();
        $("#Reviews").hide();
        $("#" + selected).show();
    });
    
});